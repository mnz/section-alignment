# section-alignment
Align section headings in source wikipedia language to section headings in target wikipedia language.

### Notebooks
- **sections.ipynb**: Extract section headings, section links and occurrence for all languages supported by the Content translation tool.

- **embeddings.ipynb**: Encode all distinct extracted section headings using multilingual bert.

- **features.ipynb**: Create source and target section pairs by generating all possible combinations of sections and calculating features such as embedding_similarity, co-occurrence_count for each of them.

- **model.ipynb**: Train a gradient boosted classifier using annotated section pairs, test it using section pairs from Content Translation tool and then distributedly rank all generated pairs using the trained model. 
